/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component, useState, useEffect } from 'react';
import type {Node} from 'react';
import { DataTable } from 'react-native-paper';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  TextInput
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Icon2 from 'react-native-vector-icons/MaterialIcons';

const optionsPerPage = [2, 3, 4];

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};


const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [page, setPage] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(optionsPerPage[0]);

  useEffect(() => {
    setPage(0);
  }, [itemsPerPage]);

  const [isVisible, setIsVisible] = useState(false);
  const [description, setDescription] = useState('');

  const hideModal = () => {
        setIsVisible(false);
  }

  const resetUploadModel = () => {
        setDescription('');
        this.textInput.clear();
  }

  const onPressAddNewPublisher  = () => {
    console.log(true);
        setIsVisible(true);
  }

  return (
    <SafeAreaView>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        showsHorizontalScrollIndicator={false} 
        showsVerticalScrollIndicator={false}
        >
        <View
          style={[backgroundStyle, {
            alignItems: 'center'
          }]}>
          <Section title="Publisher Module">
          </Section>
        </View>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>Publisher</DataTable.Title>
            <DataTable.Title>Location</DataTable.Title>
            <DataTable.Title>Options</DataTable.Title>
          </DataTable.Header>

          <DataTable.Row>
            <DataTable.Cell>Tata Achievers</DataTable.Cell>
            <DataTable.Cell>Coimbatore</DataTable.Cell>
            <DataTable.Cell><View style={{ marginBottom:5 }}>
                  <Image source={require('./images/Group327.png')} style={{ height: 20, width: 20 }}></Image>
            </View>
            <View style={{ marginBottom:5 }}>
                <Image source={require('./images/delete.png')} style={{ height: 20, width: 20 }}></Image>
            </View></DataTable.Cell>
          </DataTable.Row>

          <DataTable.Pagination
            page={page}
            numberOfPages={3}
            onPageChange={(page) => setPage(page)}
            label="1-2 of 6"
            optionsPerPage={optionsPerPage}
            itemsPerPage={itemsPerPage}
            setItemsPerPage={setItemsPerPage}
            showFastPagination
            optionsLabel={'Rows per page'}
          />
        </DataTable>

        <TouchableOpacity activeOpacity={.5} onPress={onPressAddNewPublisher}
              style={{ marginTop: 15, padding: 16, backgroundColor: '#49ACE0', borderRadius: 5, width: '50%', alignSelf: 'center' }}>
              <Text style={{ color: '#ffffff', fontSize: 18, alignSelf: 'center' }}>
                  Add New Publisher
              </Text>
        </TouchableOpacity>

        <Modal
                        supportedOrientations={['portrait', 'landscape']}
                        animationType="fade"
                        transparent={true}
                        visible={isVisible}
                        onRequestClose={() => {
                            setIsVisible(false)
                        }}>


                        <View style={{
                            flex: 1,
                            position: "absolute",
                            backgroundColor: '#00000055',
                            justifyContent: "center",
                            width: '100%',
                            height: '100%'
                        }}>

                            <View style={{ backgroundColor: '#ffffff', borderRadius: 10, marginLeft: 10, marginRight: 10 }}>


                                <View>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#49ACE0',
                                        borderTopLeftRadius: 10, borderTopRightRadius: 10
                                    }}>
                                        <View>
                                            <Text style={styles.contentTitle}>Add Publisher</Text>
                                        </View>
                                        <View style={{ marginTop: 18, marginRight: 13 }}>
                                            <Icon2
                                                onPress={hideModal}
                                                name="close"
                                                size={20} color={'#ffffff'} />

                                        </View>
                                    </View>



                                    <View>

                                        <View style={styles.textinputContainer}>

                                            <TextInput
                                                keyboardType="default"
                                                autoCapitalize='none'
                                                style={{ marginTop: 5, height: 50, textAlignVertical: 'top', padding: 10, fontSize: 14, fontWeight: 'normal' }}
                                                placeholder="Description"
                                                placeholderTextColor={'#BDBDBD'}
                                                //multiline={true}
                                                underlineColorAndroid="transparent"
                                                onChangeText={(text) => setDescription(text)}
                                                ref={input => { this.textInput = input }}
                                            />

                                        </View>

                                        <View
                                            style={{
                                                borderBottomColor: '#dcdcdc',
                                                borderBottomWidth: 1,
                                                marginRight: 20,
                                                marginLeft: 20
                                            }}
                                        />

                                        <View style={{ backgroundColor: '#ffffff', borderRadius: 10, marginLeft: 10, marginRight: 10 }}>
                                            <View style={[styles.rowModal, { marginTop: 50 }]}>

                                                <TouchableOpacity
                                                    onPress={() => resetUploadModel}
                                                    style={[styles.cancelContainer, styles.cancelbutton, { width: '30%' }]}>
                                                    <Text style={styles.btnText}>Reset</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity
                                                    onPress={() => addMedicalRecord}
                                                    style={[styles.buttonContainer, styles.rescheduleButton, { width: '60%' }]}>
                                                    <Text style={styles.btnText}>Submit</Text>
                                                </TouchableOpacity>

                                            </View>
                                        </View>

                                    </View>

                                </View>


                            </View>

                        </View>



                    </Modal>

      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  contentTitle: {
      padding: 16,
      color: '#ffffff',
      fontSize: 18,

  },
  textinputContainer: {
      width: '95%',
      margin: 10,
      flexDirection: 'column',
  },
  rowModal: {
      flexDirection: 'row',
      alignItems: 'center',
      borderColor: '#dcdcdc',
      backgroundColor: '#fff',
      borderBottomWidth: 1,
      padding: 10,


  },
  cancelContainer: {
      height: 40,
      marginLeft: 10,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 20,
      width: 100,
      borderRadius: 5,
  },
  cancelbutton: {
      backgroundColor: '#F9221E',
  },
  buttonContainer: {
      height: 40,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 20,
      marginLeft: 10,
      width: 150,
      borderRadius: 5,
  },
  rescheduleButton: {
      backgroundColor: '#49ACE0',
  },
});

export default App;
