import React, { Component, Fragment } from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Alert
} from 'react-native';
import { DataTable } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Toast, { DURATION } from 'react-native-easy-toast';

export default class App extends Component {

  constructor(props) {

    super(props);

    this.state = {
          page: 0,
          itemsPerPage: 3,
          visible: false,
          visible_edit: false,
          publisher: '',
          location: '',
          publisher_edit: '',
          location_edit: '',
          key_edit: '',
          myArray: [],
          myArrayEdit: [],
          PublisherData: [],
    };

    this.onPressAddNewPublisher = this.onPressAddNewPublisher.bind(this);
    this.onDeletePublisher = this.onDeletePublisher.bind(this);
    this.onEditPublisher = this.onEditPublisher.bind(this);

  }

  componentDidMount = async () => {

    try {
        
        const PublisherData = JSON.parse(await AsyncStorage.getItem('PublisherArray'));
        console.log(PublisherData);
        this.setState({ PublisherData: PublisherData });
        console.log(this.state.PublisherData);

    }
    catch (error) {
        console.log(error);

    }
  }

  onPressAddNewPublisher = () => {

      this.setState({
          visible: true,
          visible_edit: false
      });
    }


    onDeletePublisher = (key) => {

        console.log(key);
        const title = 'Delete Publisher';
        const message = 'Are you sure want to delete!';
        const buttons = [
            { text: 'Cancel', onPress: () => console.log("Cancel Pressed"), type: 'cancel' },
            { text: 'OK', onPress: () =>  this.DeleteData(key)}
        ];
        Alert.alert(title, message, buttons);
        // const newList = this.state.PublisherData.splice(key, 1);
        // console.log(this.state.PublisherData);
        // AsyncStorage.removeItem('PublisherArray');
        // AsyncStorage.setItem('PublisherArray', JSON.stringify(this.state.PublisherData));
        this.setState({
            visible: false,
            visible_edit: false
        });

    }

    DeleteData = (key) => {
        console.log("OK Pressed");
        const newList = this.state.PublisherData.splice(key, 1);
        console.log(this.state.PublisherData);
        AsyncStorage.removeItem('PublisherArray');
        AsyncStorage.setItem('PublisherArray', JSON.stringify(this.state.PublisherData));
        this.setState({
            visible: false,
            visible_edit: false
        });
    }

    onEditPublisher = (key) => {

        console.log(this.state.PublisherData[key]);
        this.setState({
          visible: false,
          visible_edit: true,
          publisher_edit: this.state.PublisherData[key]['publisher'],
          location_edit: this.state.PublisherData[key]['location'],
          key_edit: key,
        });

    }

    hideModal = () => {

        this.setState({
            visible: false,
            visible_edit: false
        });
    }

    resetData = () => {
        this.setState({
            publisher: '',
            location: '',
            publisher_edit: '',
            location_edit: '',
        });

        this.textPublisherInput.clear();
        this.textLocationInput.clear();
    }

    addPublisher = async () => {


        //this.setState({ visible: false });
        //console.log(this.state);

        if (this.state.publisher != '' && this.state.location != '') 
        {
          let formData =  { publisher: this.state.publisher, location: this.state.location};

            this.setState({ PublisherData: '' });
            const PublisherArray = JSON.parse(await AsyncStorage.getItem('PublisherArray'));
            console.log(PublisherArray);
            if (PublisherArray == null) 
            {
              this.state.myArray.push(formData);
              console.log(formData);
              console.log(this.state.myArray);
              await AsyncStorage.setItem('PublisherArray', JSON.stringify(this.state.myArray));
            }
            else
            {
              console.log(formData); 
              this.setState({ myArray: PublisherArray });
              this.state.myArray.push(formData);
              console.log(this.state.myArray);
              await AsyncStorage.setItem('PublisherArray', JSON.stringify(this.state.myArray));
              
            }
            
            const PublisherData = JSON.parse(await AsyncStorage.getItem('PublisherArray'));
            console.log(PublisherData);
            this.setState({ PublisherData: PublisherData });
            console.log(this.state.PublisherData);
            this.setState({
                publisher: '',
                location: '',
                publisher_edit: '',
                location_edit: '',
                myArray: [],
                myArrayEdit: [],
                visible: false,
                visible_edit: false,
            });

            this.refs.toast.show("Successfully added publisher and location", 5000);
            alert("Successfully added publisher and location");
        }

        else if(this.state.publisher != '' && this.state.location == '')
        {
          alert("Please enter the location");
          this.refs.toast.show("Please enter the location", 5000);
        }

        else if(this.state.publisher == '' && this.state.location != '')
        {
          alert("Please enter the publisher");
          this.refs.toast.show("Please enter the publisher", 5000);
        }

        else 
        {
          if(this.state.publisher == '' && this.state.location == '')
          {
            alert("Please enter the publisher and location");
            this.refs.toast.show("Please enter the publisher and location", 5000);
          }
        }
    }

    editPublisher = async () => {


        //this.setState({ visible: false });
        console.log("key_edit");
        console.log(this.state.key_edit);

        if (this.state.publisher_edit != '' && this.state.location_edit != '') 
        {
          let formDataEdit =  { publisher: this.state.publisher_edit, location: this.state.location_edit};

            this.setState({ PublisherData: '' });
            const PublisherArray = JSON.parse(await AsyncStorage.getItem('PublisherArray'));
            console.log(PublisherArray);
            if (PublisherArray == null) 
            {
              this.state.myArrayEdit.push(formDataEdit);
              console.log(formDataEdit);
              console.log(this.state.myArrayEdit);
              await AsyncStorage.setItem('PublisherArray', JSON.stringify(this.state.myArrayEdit));
            }
            else
            {
              console.log("formDataEdit"); 
              console.log(formDataEdit); 
              this.setState({ myArrayEdit: PublisherArray });

              this.state.myArrayEdit.push(formDataEdit);
              console.log("myArrayEdit");
              console.log(this.state.myArrayEdit);
              const removeItem = this.state.myArrayEdit.splice(this.state.key_edit, 1);
              console.log("removeItem");
              console.log(removeItem);
              await AsyncStorage.setItem('PublisherArray', JSON.stringify(this.state.myArrayEdit));
              
            }
            
            const PublisherData = JSON.parse(await AsyncStorage.getItem('PublisherArray'));
            console.log(PublisherData);
            this.setState({ PublisherData: PublisherData });
            console.log(this.state.PublisherData);
            this.setState({
                publisher: '',
                location: '',
                publisher_edit: '',
                location_edit: '',
                myArrayEdit: [],
                myArrayEdit: [],
                visible: false,
                visible_edit: false,
            });

            this.refs.toast.show("Successfully updated publisher and location", 5000);
            alert("Successfully updated publisher and location");
        }

        else if(this.state.publisher != '' && this.state.location == '')
        {
          alert("Please enter the location");
          this.refs.toast.show("Please enter the location", 5000);
        }

        else if(this.state.publisher == '' && this.state.location != '')
        {
          alert("Please enter the publisher");
          this.refs.toast.show("Please enter the publisher", 5000);
        }

        else 
        {
          if(this.state.publisher == '' && this.state.location == '')
          {
            alert("Please enter the publisher and location");
            this.refs.toast.show("Please enter the publisher and location", 5000);
          }
        }
    }
  

  

  render() {
    return (
      <Fragment>
      <StatusBar barStyle={'light-content'} />
      <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "height" : null} style={{ flex: 1 }}>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        showsHorizontalScrollIndicator={false} 
        showsVerticalScrollIndicator={false}
        >
        <View
          style={{
            alignItems: 'center', backgroundColor: Colors.lighter
          }}>
          <View style={styles.sectionContainer}>
          <Text
            style={[
              styles.sectionTitle,
              {
                color: Colors.black,
              },
            ]}>
            Publisher Module
          </Text>
          <Text
          style={[
            styles.sectionDescription,
            {
              color: Colors.light,
            },
          ]}>
        </Text>
        </View>
        </View>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>Publisher</DataTable.Title>
            <DataTable.Title>Location</DataTable.Title>
            <DataTable.Title>Options</DataTable.Title>
          </DataTable.Header>

          {

          this.state.PublisherData && this.state.PublisherData.length > 0 ? 
            this.state.PublisherData.map((element, key) => {
              console.log(element);
              console.log(key);
               return <DataTable.Row key={key}>
                  <DataTable.Cell>{element.publisher}</DataTable.Cell>
                  <DataTable.Cell>{element.location}</DataTable.Cell>
                  <DataTable.Cell>
                  <TouchableOpacity activeOpacity={.5} onPress={() => this.onEditPublisher(key)}>
                  <View style={{ marginBottom:5 }}>
                        <Image source={require('./images/Group327.png')} style={{ height: 20, width: 20 }}></Image>
                  </View>
                  </TouchableOpacity>
                  <TouchableOpacity activeOpacity={.5} onPress={() => this.onDeletePublisher(key)}>
                  <View style={{ marginBottom:5 }}>
                      <Image source={require('./images/delete.png')} style={{ height: 20, width: 20 }}></Image>
                  </View>
                  </TouchableOpacity>
                  </DataTable.Cell>
               </DataTable.Row>;
            }) : <DataTable.Row>
                  <DataTable.Cell></DataTable.Cell>
                  <DataTable.Cell>No Records Found</DataTable.Cell>
                  <DataTable.Cell></DataTable.Cell>
                </DataTable.Row>
          }

          
        </DataTable>

        <TouchableOpacity activeOpacity={.5} onPress={this.onPressAddNewPublisher}
              style={{ marginTop: 15, padding: 16, backgroundColor: '#49ACE0', borderRadius: 5, width: '50%', alignSelf: 'center' }}>
              <Text style={{ color: '#ffffff', fontSize: 18, alignSelf: 'center' }}>
                  Add New Publisher
              </Text>
        </TouchableOpacity>

        

        <Modal
                        supportedOrientations={['portrait', 'landscape']}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.visible}
                        onRequestClose={() => {
                            this.setState({ visible: false })
                        }}>


                        <View style={{
                            flex: 1,
                            position: "absolute",
                            backgroundColor: '#00000055',
                            justifyContent: "center",
                            width: '100%',
                            height: '100%'
                        }}>

                            <View style={{ backgroundColor: '#ffffff', borderRadius: 10, marginLeft: 10, marginRight: 10 }}>


                                <View>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#49ACE0',
                                        borderTopLeftRadius: 10, borderTopRightRadius: 10
                                    }}>
                                        <View>
                                            <Text style={styles.contentTitle}>Add Publisher</Text>
                                        </View>
                                        <View style={{ marginTop: 18, marginRight: 13 }}>
                                            <Icon2
                                                onPress={this.hideModal}
                                                name="close"
                                                size={20} color={'#ffffff'} />

                                        </View>
                                    </View>



                                    <View>

                                        <View style={styles.textinputContainer}>

                                            <TextInput
                                                keyboardType="default"
                                                autoCapitalize='none'
                                                style={{ marginTop: 5, height: 50, textAlignVertical: 'top', padding: 10, fontSize: 14, fontWeight: 'normal' }}
                                                placeholder="Publisher"
                                                placeholderTextColor={'#BDBDBD'}
                                                //multiline={true}
                                                underlineColorAndroid="transparent"
                                                onChangeText={(text) => this.setState({ publisher: text })}
                                                ref={input => { this.textPublisherInput = input }}
                                            />

                                            <View
                                                style={{
                                                    borderBottomColor: '#dcdcdc',
                                                    borderBottomWidth: 1,
                                                    marginRight: 20,
                                                    marginLeft: 20
                                                }}
                                            />

                                            <TextInput
                                                keyboardType="default"
                                                autoCapitalize='none'
                                                style={{ marginTop: 5, height: 50, textAlignVertical: 'top', padding: 10, fontSize: 14, fontWeight: 'normal' }}
                                                placeholder="Location"
                                                placeholderTextColor={'#BDBDBD'}
                                                //multiline={true}
                                                underlineColorAndroid="transparent"
                                                onChangeText={(text1) => this.setState({ location: text1 })}
                                                ref={input => { this.textLocationInput = input }}
                                            />

                                        </View>

                                        <View
                                            style={{
                                                borderBottomColor: '#dcdcdc',
                                                borderBottomWidth: 1,
                                                marginRight: 20,
                                                marginLeft: 20
                                            }}
                                        />

                                        <View style={{ backgroundColor: '#ffffff', borderRadius: 10, marginLeft: 10, marginRight: 10 }}>
                                            <View style={[styles.rowModal, { marginTop: 50 }]}>

                                                <TouchableOpacity
                                                    onPress={() => this.resetData()}
                                                    style={[styles.cancelContainer, styles.cancelbutton, { width: '30%' }]}>
                                                    <Text style={styles.btnText}>Reset</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity
                                                    onPress={() => this.addPublisher()}
                                                    style={[styles.buttonContainer, styles.rescheduleButton, { width: '60%' }]}>
                                                    <Text style={styles.btnText}>Submit</Text>
                                                </TouchableOpacity>

                                            </View>
                                        </View>

                                    </View>

                                </View>


                            </View>

                        </View>



                    </Modal>

                    <Modal
                        supportedOrientations={['portrait', 'landscape']}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.visible_edit}
                        onRequestClose={() => {
                            this.setState({ visible_edit: false })
                        }}>


                        <View style={{
                            flex: 1,
                            position: "absolute",
                            backgroundColor: '#00000055',
                            justifyContent: "center",
                            width: '100%',
                            height: '100%'
                        }}>

                            <View style={{ backgroundColor: '#ffffff', borderRadius: 10, marginLeft: 10, marginRight: 10 }}>


                                <View>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#49ACE0',
                                        borderTopLeftRadius: 10, borderTopRightRadius: 10
                                    }}>
                                        <View>
                                            <Text style={styles.contentTitle}>Edit Publisher</Text>
                                        </View>
                                        <View style={{ marginTop: 18, marginRight: 13 }}>
                                            <Icon2
                                                onPress={this.hideModal}
                                                name="close"
                                                size={20} color={'#ffffff'} />

                                        </View>
                                    </View>



                                    <View>

                                        <View style={styles.textinputContainer}>

                                            <TextInput
                                                keyboardType="default"
                                                autoCapitalize='none'
                                                style={{ marginTop: 5, height: 50, textAlignVertical: 'top', padding: 10, fontSize: 14, fontWeight: 'normal' }}
                                                placeholder="Publisher"
                                                placeholderTextColor={'#BDBDBD'}
                                                //multiline={true}
                                                underlineColorAndroid="transparent"
                                                value={this.state.publisher_edit}
                                                onChangeText={(text) => this.setState({ publisher_edit: text })}
                                                ref={input => { this.textPublisherInput = input }}
                                            />

                                            <View
                                                style={{
                                                    borderBottomColor: '#dcdcdc',
                                                    borderBottomWidth: 1,
                                                    marginRight: 20,
                                                    marginLeft: 20
                                                }}
                                            />

                                            <TextInput
                                                keyboardType="default"
                                                autoCapitalize='none'
                                                style={{ marginTop: 5, height: 50, textAlignVertical: 'top', padding: 10, fontSize: 14, fontWeight: 'normal' }}
                                                placeholder="Location"
                                                placeholderTextColor={'#BDBDBD'}
                                                //multiline={true}
                                                underlineColorAndroid="transparent"
                                                value={this.state.location_edit}
                                                onChangeText={(text1) => this.setState({ location_edit: text1 })}
                                                ref={input => { this.textLocationInput = input }}
                                            />

                                        </View>

                                        <View
                                            style={{
                                                borderBottomColor: '#dcdcdc',
                                                borderBottomWidth: 1,
                                                marginRight: 20,
                                                marginLeft: 20
                                            }}
                                        />

                                        <View style={{ backgroundColor: '#ffffff', borderRadius: 10, marginLeft: 10, marginRight: 10 }}>
                                            <View style={[styles.rowModal, { marginTop: 50 }]}>

                                                <TouchableOpacity
                                                    onPress={() => this.resetData()}
                                                    style={[styles.cancelContainer, styles.cancelbutton, { width: '30%' }]}>
                                                    <Text style={styles.btnText}>Reset</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity
                                                    onPress={() => this.editPublisher()}
                                                    style={[styles.buttonContainer, styles.rescheduleButton, { width: '60%' }]}>
                                                    <Text style={styles.btnText}>Update</Text>
                                                </TouchableOpacity>

                                            </View>
                                        </View>

                                    </View>

                                </View>


                            </View>

                        </View>



                    </Modal>

                    

      </ScrollView>
      <Toast
                ref="toast"
                style={{ backgroundColor: Colors.grey_8 }}
                position='bottom' 
            />
      </KeyboardAvoidingView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  contentTitle: {
      padding: 16,
      color: '#ffffff',
      fontSize: 18,

  },
  textinputContainer: {
      width: '95%',
      margin: 10,
      flexDirection: 'column',
  },
  rowModal: {
      flexDirection: 'row',
      alignItems: 'center',
      borderColor: '#dcdcdc',
      backgroundColor: '#fff',
      borderBottomWidth: 1,
      padding: 10,


  },
  cancelContainer: {
      height: 40,
      marginLeft: 10,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 20,
      width: 100,
      borderRadius: 5,
  },
  cancelbutton: {
      backgroundColor: '#F9221E',
  },
  buttonContainer: {
      height: 40,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 20,
      marginLeft: 10,
      width: 150,
      borderRadius: 5,
  },
  rescheduleButton: {
      backgroundColor: '#49ACE0',
  },
});